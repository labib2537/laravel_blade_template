<h1> Sayed Farhan Labib Karib 
</h1>

<h2>Hello, {{$name}}</h2>
<h2>{{$name}} is {{$age}} years old</h2>
<h2>From {{$address}} </h2>

@if($age%2==0)
<h3>Even Number</h3>
@else
<h3>Odd Number</h3>
@endif

@foreach($datas as $data)
<h4>{{$data}}</h4>
@endforeach

<h3>Multiplication Table of {{$age}} </h3>
@for($i=1;$i<11;$i++)
<h4>{{$age}} X {{$i}} = {{$age*$i}}</h4>
@endfor